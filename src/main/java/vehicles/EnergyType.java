package vehicles;

public enum EnergyType {
    GASOLINE(70, 10000),
    ELECTRICITY(15, 30000),
    GAS(35, 20000);

    private final int costPerMile;
    private final int engineCost;

    EnergyType(int costPerMile, int engineCost) {
        this.costPerMile = costPerMile;
        this.engineCost = engineCost;
    }

    public int getCostPerMile() {
        return costPerMile;
    }

    public int getEngineCost() {
        return engineCost;
    }
}
