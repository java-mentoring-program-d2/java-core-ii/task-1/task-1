package vehicles;

public class GasBus extends Vehicle {

    public GasBus(int mass, int maxSpeed, int capacity) {
        super(mass, maxSpeed, capacity);
    }
    //Overriding getType() method from Vehicle class
    @Override
    EnergyType getType() {
        return EnergyType.GAS;
    }

}
